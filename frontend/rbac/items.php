<?php
return [
    'guest' => [
        'type' => 1,
        'children' => [
            'product_view',
        ],
    ],
    'moderator' => [
        'type' => 1,
        'children' => [
            'guest',
            'product_confirm_create',
            'product_confirm_update',
        ],
    ],
    'editor' => [
        'type' => 1,
        'children' => [
            'guest',
            'product_create',
            'product_update',
        ],
    ],
    'product_create' => [
        'type' => 2,
    ],
    'product_update' => [
        'type' => 2,
    ],
    'product_confirm_create' => [
        'type' => 2,
    ],
    'product_confirm_update' => [
        'type' => 2,
    ],
    'product_view' => [
        'type' => 2,
    ],
];
