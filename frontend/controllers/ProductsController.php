<?php

namespace frontend\controllers;

use Yii;
use common\models\Products;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['guest'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','update'],
                        'roles' => ['editor'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['published','changed'],
                        'roles' => ['moderator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'published' => ['POST'],
                    'changed' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Products::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $model->setScenario(Products::SCENARIO_INSERT);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(Products::SCENARIO_UPDATE);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        if (!is_null($model->change_name)) {
            $model->name = $model->change_name;
        }

        if (!is_null($model->change_price_rub)) {
            $model->price_rub = $model->change_price_rub;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Изменяется состояние товара с не опубликованно на опубликованно
     *
     * @param $id ID товара
     * @return \yii\web\Response Шаблон главной страницы
     * @throws NotFoundHttpException Если модель не найдена
     */
    public function actionPublished($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(Products::SCENARIO_MODERATOR);
        $model->setPublished()->save();

        return $this->redirect(['index']);
    }

    /**
     * Применяются внесенные правки в товар
     *
     * @param $id ID товара
     * @return \yii\web\Response Шаблон главной страницы
     * @throws NotFoundHttpException Если модель не найдена
     */
    public function actionChanged($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(Products::SCENARIO_MODERATOR);
        $model->setChanged()->save();

        return $this->redirect(['index']);
    }
}
