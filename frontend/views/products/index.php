<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */


$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('product_create')): ?>
    <p>
        <?= Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'value' => function($model, $key, $index) {
                    if (Yii::$app->user->can('moderator') || Yii::$app->user->can('editor')) {
                        if ($model->changed && $model['change_name'] != $model['name']) {
                            return Html::tag('strong', $model['change_name']) . '<br>' . Html::tag('s', $model['name']);
                        }
                    }

                    return $model['name'];
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'price_rub',
                'value' => function($model, $key, $index) {
                    if (Yii::$app->user->can('moderator') || Yii::$app->user->can('editor')) {
                        if ($model->changed && $model['change_price_rub'] != $model['price_rub']) {
                            return Html::tag('strong', $model['change_price_rub']) . '<br>' . Html::tag('s', $model['price_rub']);
                        }
                    }

                    return $model['price_rub'];
                },
                'format' => 'html',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => '{published}  {changed}  {update}',
                'visible' => Yii::$app->user->can('moderator') || Yii::$app->user->can('editor'),
                'buttons' => [
                    'published' => function($url, $model) {
                        $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-ok']);
                        return Html::a($icon, $url, ['data-method' => 'post', 'title' => 'Опубликовать']);
                    },
                    'changed' => function($url, $model) {
                        $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-retweet']);
                        return Html::a($icon, $url, ['data-method' => 'post', 'title' => 'Применить изменения']);
                    },
                ],
                'visibleButtons' => [
                    'published' => function($model) {
                        return Yii::$app->user->can('product_confirm_create') && $model->published === 0;
                    },
                    'changed' => function($model) {
                        return Yii::$app->user->can('product_confirm_update') && $model->changed === 1;
                    },
                    'update' => Yii::$app->user->can('product_update'),
                ],
            ],
        ],
    ]); ?>
</div>
