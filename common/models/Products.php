<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Модель для таблицы "{{%products}}".
 *
 * @property string $id ID
 * @property string $name Название
 * @property string $price_rub Цена в рублях
 * @property int $published Опубликованный
 * @property int $changed Измененный
 * @property string $change_name Измененнное название
 * @property string $change_price_rub Измененнная цена в рублях
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * Константа для сценария при добавлении новых данных
     */
    const SCENARIO_INSERT = 'insert';

    /**
     * Константа для сценария при обновлении данных
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * Константа для сценария при обновлении данных
     */
    const SCENARIO_MODERATOR = 'moderator';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_INSERT => ['name', 'price_rub'],
            self::SCENARIO_UPDATE => ['change_name', 'change_price_rub'],
            self::SCENARIO_MODERATOR => ['published', 'changed'],
        ];
    }

    /**
     * Создаем дополнительные условия поиска в зависимости от прав пользователя
     * @inheritdoc
     */
    public static function find()
    {
        $res = parent::find();
        if (Yii::$app->user->isGuest) {
            $res->andWhere(['published' => 1]);
        }
        return $res;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price_rub'], 'required', 'on' => self::SCENARIO_INSERT],
            [['change_name', 'change_price_rub'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['price_rub', 'change_price_rub'], 'number', 'min' => 0],
            [['name', 'change_name'], 'string', 'max' => 255],
            [['published', 'changed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price_rub' => 'Цена в рублях',
            'published' => 'Опубликованный',
            'changed' => 'Измененный',
            'change_name' => 'Измененнное название',
            'change_price_rub' => 'Измененнная цена в рублях',
        ];
    }

    /**
     * Устанавливает атрибут "опубликованно" и возвращает текущий объект
     *
     * @return $this
     */
    public function setPublished()
    {
        $this->published = true;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if ($this->getIsNewRecord()) {
            return parent::load($data, $formName);
        }

        if (empty($data)) {
            return false;
        }

        $this->change_name = ArrayHelper::getValue($data, $this->formName() . '.name');
        $this->change_price_rub = ArrayHelper::getValue($data, $this->formName() . '.price_rub');
        if (
            $this->change_name
            && $this->change_price_rub
            && (
                $this->name !== $this->change_name
                || $this->price_rub !== $this->change_price_rub
            )
        ) {
            $this->changed = true;
        }

        return true;
    }

    /**
     * Устанавливает атрибут "изменено", заменят change_ атрибуты
     * и возвращает текущий объект
     *
     * @return $this
     */
    public function setChanged()
    {
        $this->name = $this->change_name;
        $this->price_rub = $this->change_price_rub;
        $this->changed = false;

        return $this;
    }
}
