<?php

use yii\db\Migration;

/**
 * Миграция создает необходимые права контроля доступа на основе ролей
 * Роли
 *     moderator - Может подтверждать создание нового товара, принимать изменения атрибутов
 *                 товара и просматривать товары
 *     editor - Может создавать новый товар, изменять атрибуты товара и просматривать товары
 *     guest - Просматривать товры
 */
class m180412_200035_create_rbac_param extends Migration
{
    public function __construct(array $config = [])
    {
        Yii::$app->setBasePath(dirname(dirname(__DIR__)) . '/frontend');
        parent::__construct($config);
    }

    public function up()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $guest = $auth->createRole('guest');
        $moderator = $auth->createRole('moderator');
        $editor = $auth->createRole('editor');

        // Создание новых товаров
        $perCreate = $auth->createPermission('product_create');

        // Редактирование товара
        $perUpdate = $auth->createPermission('product_update');

        // Подтверждение при создании нового товара
        $perConfCreate = $auth->createPermission('product_confirm_create');

        // Подтверждение изменения товара
        $perConfUpdate = $auth->createPermission('product_confirm_update');

        // Просмотр товара(ов)
        $perView = $auth->createPermission('product_view');

        $auth->add($guest);
        $auth->add($moderator);
        $auth->add($editor);
        $auth->add($perCreate);
        $auth->add($perUpdate);
        $auth->add($perConfCreate);
        $auth->add($perConfUpdate);
        $auth->add($perView);

        $auth->addChild($guest, $perView);
        $auth->addChild($moderator, $guest);
        $auth->addChild($editor, $guest);
        $auth->addChild($moderator, $perConfCreate);
        $auth->addChild($moderator, $perConfUpdate);
        $auth->addChild($editor, $perCreate);
        $auth->addChild($editor, $perUpdate);

        $query = Yii::$app->db->createCommand("SELECT id FROM {{%user}} WHERE username = :username");

        $auth->assign($moderator, $query->bindValue(':username', 'moderator')->queryScalar());
        $auth->assign($editor, $query->bindValue(':username', 'editor')->queryScalar());
    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }
}
