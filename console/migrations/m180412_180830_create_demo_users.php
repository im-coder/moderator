<?php

use yii\db\Migration;

/**
 * Создание временных, тестовых пользователей
 */
class m180412_180830_create_demo_users extends Migration
{
    private $userlist = array('moderator','editor');

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ($this->userlist as $username) {
            $this->insert('{{%user}}', [
                'username' => $username,
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('12345'),
                'email' => "$username@example.com",
                'status' => 10,
                'created_at' => time()
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'username', $this->userlist]);
    }
}
