<?php

use yii\db\Migration;

/**
 * Создание таблицы для хранения продуктов
 */
class m180413_043342_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(11)->unsigned()->comment('ID'),
            'name' => $this->string(255)->notNull()->comment('Название'),
            'price_rub' => $this->money(11, 2)->notNull()->comment('Цена в рублях'),
            'published' => $this->boolean()->defaultValue(0)->notNull()->comment('Опубликованный'),
            'changed' => $this->boolean()->defaultValue(0)->notNull()->comment('Измененный'),
            'change_name' => $this->string(255)->null()->comment('Измененнное название'),
            'change_price_rub' => $this->money(11, 2)->null()->comment('Измененнная цена в рублях'),
        ]);
        $this->createIndex('published', '{{%products}}', 'published');
        $this->createIndex('changed', '{{%products}}', 'changed');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }
}
